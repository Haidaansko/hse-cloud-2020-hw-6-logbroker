import asyncio
import csv
import json
import os
import ssl
import sys
import time
import uuid
import pathlib
from io import StringIO

from aiohttp.client import ClientSession
from aiohttp.client_exceptions import ClientError
from fastapi import FastAPI, Request, Response

LOG_DIR = pathlib.Path('/logs')
CH_FORMAT_MAPPING = {'list' : 'CSV', 'json' : 'JSONEachRow'}

CH_HOST = os.getenv('LOGBROKER_CH_HOST', 'localhost')
CH_USER = os.getenv('LOGBROKER_CH_USER')
CH_PASSWORD = os.getenv('LOGBROKER_CH_PASSWORD')
CH_PORT = int(os.getenv('LOGBROKER_CH_PORT', 8123))
CH_CERT_PATH = os.getenv('LOGBROKER_CH_CERT_PATH')


async def execute_query(query, data=None):
    url = f'http://{CH_HOST}:{CH_PORT}/'
    params = {
        'query': query.strip()
    }
    headers = {}
    if CH_USER is not None:
        headers['X-ClickHouse-User'] = CH_USER
        if CH_PASSWORD is not None:
            headers['X-ClickHouse-Key'] = CH_PASSWORD
    ssl_context = ssl.create_default_context(cafile=CH_CERT_PATH) if CH_CERT_PATH is not None else None

    async with ClientSession() as session:
        async with session.post(url, params=params, data=data, headers=headers, ssl=ssl_context) as resp:
            await resp.read()
            try:
                resp.raise_for_status()
                return resp, None
            except ClientError as e:
                return resp, {'error': str(e)}


app = FastAPI()


async def query_wrapper(query, data=None):
    res, err = await execute_query(query, data)
    if err is not None:
        return err
    return await res.text()


@app.get('/show_create_table')
async def show_create_table(table_name: str):
    resp = await query_wrapper(f'SHOW CREATE TABLE "{table_name}";')
    if isinstance(resp, str):
        return Response(content=resp.replace('\\n', '\n'), media_type='text/plain; charset=utf-8')
    return resp


@app.post('/write_log')
async def write_log(request: Request):
    FORMATS = ('list', 'json')
    body = await request.json()
    res = []
    for log_entry in body:
        table_name = log_entry['table_name']
        rows = log_entry['rows']
        log_format = log_entry.get('format')
        if log_format in FORMATS:
            res.append(cache_log(log_format, table_name, rows))
        else:
            res.append({'error': f'unknown format {log_entry.get("format")}, you must use list or json'})
    return res


@app.get('/healthcheck')
async def healthcheck():
    return Response(content='Ok', media_type='text/plain')


@app.get('/status')
async def status(query_id: str):
    if list(LOG_DIR.glob(query_id)):
        return 'Query is cached'
    return 'Query is not in cache'



def cache_log(log_format, table_name, rows):
    dir_name = str(uuid.uuid4())
    p = LOG_DIR / dir_name
    p.mkdir(parents=True, exist_ok=True)

    fname = f'{table_name}_{log_format}.log'
    print(f'Writing to {p / fname}')
    with open(p / fname, 'a') as f:
        if log_format == 'list':
            cwr = csv.writer(f, quoting=csv.QUOTE_ALL)
            cwr.writerows(rows)
        else:
            for row in rows:
                assert isinstance(row, dict)
                f.write(json.dumps(row))
                f.write('\n')
    return p.name


async def dump_cache():
    THRESHOLD = 5
    ts_cur = time.time()

    file_ts = list(map(lambda x: (x, os.path.getmtime(x)), LOG_DIR.iterdir()))
    
    for path, ts in sorted(file_ts, key=lambda x : x[1]):
        if ts_cur - ts < THRESHOLD:
            break

        log_file = next(path.iterdir())
        table_name, log_format = log_file.name.rsplit('.', 1)[0].rsplit('_', 1)

        print(f'Uploading {path.name} to ClickHouse')
        with open(log_file, 'r') as f:
            data = f.read()
        task = asyncio.create_task(query_wrapper(f'INSERT INTO \"{table_name}\" FORMAT {CH_FORMAT_MAPPING[log_format]}', data))
        done, _ = await asyncio.wait({task}, timeout=0.1)
        if task in done:
            log_file.unlink()
            path.rmdir()



async def dumping_cache():
    while True:
        await asyncio.sleep(10)
        await dump_cache()

@app.on_event("startup")
def startup():
    asyncio.create_task(dumping_cache())


if __name__ == '__main__':
    import uvicorn

    uvicorn.run('server:app', host='0.0.0.0', port=8000, reload=True, debug=True)

