#!/usr/bin/env bash

cd $(dirname $0)/..

docker-compose build
docker-compose up -d clickhouse logbroker 
docker-compose run clickhouse-client -q 'create table kek (a Int32, b String) ENGINE = MergeTree() primary key a;'
docker-compose run curl "logbroker:8000/show_create_table?table_name=kek"